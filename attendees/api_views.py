from django.http import JsonResponse

from attendees.models import Attendee
from events.models import Conference
from common.json import ModelEncoder
from django.views.decorators.http import require_http_methods
import json


class ListAttendeesEncoder(ModelEncoder):
    model = Attendee
    properties = ["email", "name"]


@require_http_methods(["POST", "GET"])
def api_list_attendees(request, conference_id):
    if request.method == "GET":
        list_attendees = Attendee.objects.filter(conference_id=conference_id)
        return JsonResponse(
            list_attendees, encoder=ListAttendeesEncoder, safe=False
        )
    else:
        content = json.loads(request.body)

        # Get the Conference object and put it in the content dict
        try:
            conference = Conference.objects.get(id=conference_id)
            content["conference"] = conference
        except Conference.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid conference id"},
                status=400,
            )

        attendee = Attendee.objects.create(**content)
        return JsonResponse(attendee, encoder=ListAttendeesEncoder, safe=False)


class ShowAttendee(ModelEncoder):
    model = Attendee
    properties = [
        "email",
        "name",
        "company_name",
    ]


@require_http_methods(["DELETE", "GET"])
def api_show_attendee(request, id):
    if request.method == "GET":
        try:
            show_attendee = Attendee.objects.get(id=id)
        except Attendee.DoesNotExist:
            JsonResponse({"message": "id is not exist"}, status=400)
        return JsonResponse(show_attendee, encoder=ShowAttendee, safe=False)
    else:
        code, lis = show_attendee = Attendee.objects.get(id=id).delete()
        return JsonResponse({"delet": 0 < code, "list_deleted_content": lis})


class List_AllAttendee(ModelEncoder):
    model = Attendee
    properties = [
        "email",
        "name",
        "company_name",
        "created",
    ]

    # def get_extra_data(self, o):
    #     return {"conference": obj.conference.id for obj in o}

    "s"


@require_http_methods(["POST", "GET"])
def list_all_attendee(request):
    if request.method == "GET":
        attendee = Attendee.objects.all()
        return JsonResponse(attendee, encoder=List_AllAttendee, safe=False)
    else:
        content = json.loads(request.body)

        try:
            if "conference" in content:
                conference = Conference.objects.get(id=content["conference"])
                content["conference"] = conference
        except Conference.DoesNotExist:
            return JsonResponse({"message": "id is not esxist"}, status=400)

        attendee = Attendee.objects.create(**content)
    return JsonResponse(attendee, encoder=List_AllAttendee, safe=False)
