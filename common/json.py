from json import JSONEncoder
from datetime import datetime
from django.db.models import QuerySet


class DateEncoder(JSONEncoder):
    def default(self, o):
        if isinstance(o, datetime):
            return o.isoformat()
        else:
            return super().default(o)


class QuerySetEncoder(JSONEncoder):
    def default(self, o):
        if isinstance(o, QuerySet):
            # print(
            #     o,
            #     "----------------------------------class QuerySetEncoder(JSONEncoder):--------------------------",
            # )
            return list(o)
        return super().default(o)


class ModelEncoder(DateEncoder, QuerySetEncoder, JSONEncoder):
    encoders = {}
    # print(
    #     encoders,
    #     "----------------------------------Encoders--------------------------",
    # )

    def default(self, o):
        #   if the object to decode is the same class as what's in the
        #   model property, then
        if isinstance(o, self.model):
            # print(
            #     o,
            #     type(o),
            #     "----------------------------------o--------------------------",
            # )

            # print(
            #     self.model,
            #     "----------------------------------self.model--------------------------",
            # ),
            #     * create an empty dictionary that will hold the property name
            d = {}
            # creating a href exception
            if hasattr(o, "get_api_url"):
                d["href"] = o.get_api_url()
            #       as keys and the property values as values
            #     * for each name in the properties list
            for property in self.properties:
                # print(
                #     property,
                #     type(property),
                #     "----------------------------------property--------------------------",
                # )
                value = getattr(o, property)
                # print(
                #     value,
                #     "----------------------------------value--------------------------",
                # )
                d[property] = value

            #         * get the value of that property from the model instance
            #           given just the property name
            #         * put it into the dictionary with that property name as
            #           the key
            #     * return the dictionary
            # print(
            #     d,
            #     "----------------------------------d--------------------------",
            # )
            d.update(self.get_extra_data(o))
            return d

        #   otherwise,
        #       return super().default(o)  # From the documentation
        else:
            e = super().default(o)
            # print(
            #     e,
            #     type(e),
            #     "---------------------------------- e = super().default(o)--------------------------",
            # )
            return e

    def get_extra_data(self, o):
        # print(
        #     "---------------------------------- def get_extra_data(self, o):----at json----------------------",
        # )
        return {}
