FROM python:3
# defines and downloads the image this image is based on

ENV PYTHONUNBUFFERED 1
#DEFINES ENVIRONMENTA VARIABLES

WORKDIR / app
# defines the working directory for our cod in th container

COPY ..
#copies code from current local folder to the image

RUN pip install -r requirements.txt
#runs a command within the container commandline

CMD python manage.py runserver "0.0.0.0:8000"
#runs
