from .key import PEXELS_API_KEY
import requests


def get_photo(city, state):
    # create the dictionary for the header
    header = {"Authorization": PEXELS_API_KEY}
    # create the URL or the report
    query = f"{city} {state}"
    url = f"https://api.pexels.com/v1/search?query={query}"
    # make the requeest
    response = requests.get(url, headers=header)

    # parse the JSON request
    # retuen a dictionary that contain a icture_URL key and
    # one of the URLs oen for the pictures in the response
    return {"picture_url": response.json()["photos"][0]["src"]["original"]}


print(get_photo("kearney", "Nebraska"))
