from django.http import JsonResponse
from common.json import ModelEncoder
from .models import Conference, Location, State
from django.views.decorators.http import require_http_methods
import json
from .acls import get_photo


class ListConferencesEncoder(ModelEncoder):
    model = Conference
    properties = [
        "name",
        "starts",
        "ends",
        "description",
        "max_presentations",
        "max_attendees",
    ]

    def get_extra_data(self, o):
        return {"location": o.location.id}


@require_http_methods(["GET", "POST"])
def api_list_conferences(request):
    if request.method == "GET":
        conferences = Conference.objects.all()
        return JsonResponse(
            conferences, encoder=ListConferencesEncoder, safe=False
        )
    else:
        # change the json body into python dictionary using json.loads
        content = json.loads(request.body)
        # creating exceprion incease the location is not exist
        try:
            # getting the object location with a specifict id
            location = Location.objects.get(id=content["location"])
            # input the object that is pulled into location instace into the content dict
            content["location"] = location
        except Location.DoesNotExist:
            return JsonResponse({"message": "invalid location id"}, status=400)
        conference = Conference.objects.create(**content)
        return JsonResponse(
            conference, encoder=ListConferencesEncoder, safe=False
        )


class ConferenceDetailEncoder(ModelEncoder):
    model = Conference
    properties = [
        "name",
        "description",
        "max_presentations",
        "max_attendees",
        "starts",
        "ends",
        "created",
        "updated",
    ]


def api_show_conference(request, id):
    conference = Conference.objects.get(id=id)
    return JsonResponse(
        conference, encoder=ConferenceDetailEncoder, safe=False
    )


###############


class LocationListEncoder(ModelEncoder):
    model = Location
    properties = [
        "name",
        "city",
        "room_count",
        "updated",
        "picture_url",
        "state",
    ]

    def get_extra_data(self, o):
        return {"state": o.state.abbreviation}


@require_http_methods(["GET", "POST"])
def api_list_locations(request):
    # print(
    #     request.body,
    #     "--------------------------------------------------request.body",
    # )
    # adding location into the data --> convert into pthon
    if request.method == "GET":
        location = Location.objects.all()

    else:
        content = json.loads(request.body)

        # print(
        #     content,
        #     "-----------------------------------------------------------------------content_1",
        # )
        state = State.objects.get(abbreviation=content["state"])

        # print(
        #     state,
        #     type(state),
        #     "------------------------------------------------------------------------------------------------------------state",
        # )

        content["state"] = state
        picture_url = get_photo(content["city"], content["state"].name)
        content.update(**picture_url)

        # print(
        #     content,
        #     "-----------------------------------------------------------------------------------------content_2",
        # )

        location = Location.objects.create(**content)

        # print(
        #     location,
        #     "-----------------------------------------------------------------------------------------------------------------Location",
        # )

    return JsonResponse(location, encoder=LocationListEncoder, safe=False)


#################


class LocationDetailEncoder(ModelEncoder):
    model = Location
    properties = ["name", "city", "room_count", "picture_url"]

    def get_extra_data(self, o):
        return {"state": o.state.abbreviation}


@require_http_methods(["DELETE", "GET", "PUT"])
def api_show_location(request, id):
    if request.method == "GET":
        try:
            content = Location.objects.get(id=id)
            print(
                content,
                "------------------------content----------------------",
            )
        except Location.DoesNotExist:
            return JsonResponse({"message": "location is not exsit"})
        else:
            return JsonResponse(
                content, encoder=LocationDetailEncoder, safe=False
            )

    elif request.method == "DELETE":
        count, _ = Location.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count > 0})

    else:
        # copied from create
        content = json.loads(request.body)
        try:
            # new code
            if "state" in content:
                state = State.objects.get(abbreviation=content["state"])
                content["state"] = state
        except State.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid state abbreviation"},
                status=400,
            )

        # new code
        Location.objects.filter(id=id).update(**content)

        # copied from get detail
        location = Location.objects.get(id=id)
        return JsonResponse(
            location,
            encoder=LocationDetailEncoder,
            safe=False,
        )
