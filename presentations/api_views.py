from django.http import JsonResponse

from .models import Presentation
from common.json import ModelEncoder


class ListPresentationEncoder(ModelEncoder):
    model = Presentation
    properties = ["title"]

    def get_extra_data(self, o):
        e = {"status": o.status.name}
        print(
            e,
            "-->",
            o,
            "-------------------------------------------------------------------------views  def get_extra_data(self, o):----Oooooooo------------------------------------ ",
        )
        return {"status": o.status.name}


def api_list_presentations(request, conference_id):
    # presentations = [
    #     {
    #         "title": p.title,
    #         "status": p.status.name,
    #         "href": p.get_api_url(),
    #     }
    #     for p in Presentation.objects.filter(conference=conference_id)
    presentaions = Presentation.objects.filter(conference_id=conference_id)
    return JsonResponse(
        presentaions, encoder=ListPresentationEncoder, safe=False
    )


def api_show_presentation(request, id):
    """
    Returns the details for the Presentation model specified
    by the id parameter.

    This should return a dictionary with the presenter's name,
    their company name, the presenter's email, the title of
    the presentation, the synopsis of the presentation, when
    the presentation record was created, its status name, and
    a dictionary that has the conference name and its URL

    {
        "presenter_name": the name of the presenter,
        "company_name": the name of the presenter's company,
        "presenter_email": the email address of the presenter,
        "title": the title of the presentation,
        "synopsis": the synopsis for the presentation,
        "created": the date/time when the record was created,
        "status": the name of the status for the presentation,
        "conference": {
            "name": the name of the conference,
            "href": the URL to the conference,
        }
    }
    """
    show = Presentation.objects.get(id=id)
    print(show, "-----------------------show----------------------")
    response = {
        "presenter_name": show.presenter_name,
        "company_name": show.company_name,
        "presenter_email": show.presenter_email,
        "title": show.title,
        "synopsis": show.synopsis,
        "created": show.created,
        "status": show.status,
        "conference": {
            "name": show.conference.name,
            "href": show.conference.get_api_url(),
        },
    }
    return JsonResponse(response)
